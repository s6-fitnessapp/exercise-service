FROM node:15.14.0 As development

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
RUN npm i -g @nestjs/cli

COPY . .

EXPOSE 8082
CMD ["npm", "run", "start:dev"]