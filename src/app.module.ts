import { Module } from '@nestjs/common';
import { ExerciseModule } from './exercise.module';
import {TypeOrmModule} from "@nestjs/typeorm";
import { ConfigModule } from '@nestjs/config';

console.log(process.env.STMP_CONNECTION);
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    ExerciseModule,
    ConfigModule.forRoot({
      isGlobal: true,
    })
  ],
})
export class AppModule {}
