import { Test, TestingModule } from '@nestjs/testing';
import { ExerciseController } from './exercise.controller';
import { ExerciseProducer } from '../event/exercise.producer';
import { ExerciseService } from '../service/exercise.service';
import { Deserializer } from 'jsonapi-serializer';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExerciseEntity } from '../entity/exercise.entity';
import { ExerciseRequest } from '../models/exercise.request';


describe('ExerciseController', () => {
  let controller: ExerciseController;
  let service: ExerciseService;
  let producer: ExerciseProducer;
  let deserializer: Deserializer;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExerciseController],
      providers: [ExerciseService, ExerciseProducer],
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqlite',
          database: ':memory:',
          entities: ['./**/*.entity.ts'],
          dropSchema: true,
          synchronize: true,
          logging: false,
        }),
        TypeOrmModule.forFeature([ExerciseEntity]),
      ],
    }).compile();
    deserializer = new Deserializer({});

    controller = module.get<ExerciseController>(ExerciseController);
    service = module.get<ExerciseService>(ExerciseService);
    producer = module.get<ExerciseProducer>(ExerciseProducer);
    const catReq = new ExerciseRequest();
    catReq.name = 'Population';
    await controller.create(catReq);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a Exercise', async () => {
    let exerciseRequest: ExerciseRequest = new ExerciseRequest();
    exerciseRequest.name = 'Sport'
    let ExerciseResponse = await controller.create(exerciseRequest);
    const Exercise = await deserializer.deserialize(ExerciseResponse);
    expect(Exercise.name).toMatch('Sport');
  });

/*  it('should not create a duplicate Exercise', async () => {
    let exerciseRequest: ExerciseRequest = new ExerciseRequest();
    exerciseRequest.name = 'Population'
    expect(await controller.create(exerciseRequest)).toThrow()
  });*/

  it('should get a Exercise', async () => {
    const ExerciseResponse = await controller.getById(1);
    const Exercise = await deserializer.deserialize(ExerciseResponse);
    expect(Exercise.name).toMatch('Population');
  });

/*  it('should trow when getting a nonexistant Exercise', async () => {
    expect(await controller.getById(20)).toThrow()
  });*/

  it('should get all categories', async () => {

    const ExerciseResponse = await controller.getAll();
    const Exercise = await deserializer.deserialize(ExerciseResponse);
    console.log(Exercise);
    expect(ExerciseResponse).toBeDefined();
  });

  it('should edit a Exercise', async () => {
    const exerciseResponse = await controller.getById(1);
    const exercise = await deserializer.deserialize(exerciseResponse);
    var exerciseRequest: ExerciseRequest = new ExerciseRequest()
    exerciseRequest.name = 'New name'
    exerciseRequest.id = exercise.id
    const ExerciseR = await controller.edit(exerciseRequest)
    const cat = await deserializer.deserialize(ExerciseR);
    expect(cat.name).toMatch('New name');
  });

  it('should not edit a Exercise with an existing name', async () => {
    const exerciseResponse = await controller.getById(1);
    const exercise = await deserializer.deserialize(exerciseResponse);
    let exerciseRequest: ExerciseRequest = new ExerciseRequest()
    exerciseRequest.name = 'Population'
    exerciseRequest.id = exercise.id
    const ExerciseR = await controller.edit(exerciseRequest)
    const cat = await deserializer.deserialize(ExerciseR);
    expect(cat.name).toMatch('Population');
  });
});
