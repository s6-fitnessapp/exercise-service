import { Body, Controller, Delete, Get, Param, Post, Put, Query, Res, ValidationPipe } from '@nestjs/common';

import { ExerciseService } from '../service/exercise.service';
import { ExerciseRequest } from '../models/exercise.request';
import { ExerciseResponse} from '../response/exercise.response.c';
import { Serializer, Error } from 'jsonapi-serializer';
import { ApiCreatedResponse, ApiResponse } from '@nestjs/swagger';
import {response} from "express";


@Controller('exercise')
export class ExerciseController {

  constructor(private readonly ExerciseService: ExerciseService) {
  }

  ExerciseSerializer = new Serializer('Exercise', {
    attributes: ['name'],
  });

  @Delete(':id')
  @ApiCreatedResponse({
    type: ExerciseResponse,
    isArray: false
  })
  @ApiResponse({ status: 200, description: 'The exercise has been successfully deleted.'})
  @ApiResponse({ status: 400, description: 'Failed to delete the exercise, make sure the exercise exists.'})

  delete(@Param('id') id: number): Promise<JSON> {

    return this.ExerciseSerializer.serialize(this.ExerciseService.delete(id)).catch(() =>
    {
      throw  new Error({
        code: '400',
        title: 'Bad Request',
        detail: 'Failed to delete.',
      });
    })



  }

  @Put()
  @ApiCreatedResponse({
    type: ExerciseResponse,
    isArray: false
  })
  @ApiResponse({ status: 200, description: 'The exercise has been successfully edited.'})
  @ApiResponse({ status: 400, description: 'Failed to edit the exercise, make sure the exercise exists.'})
  edit(@Body(new ValidationPipe({ skipMissingProperties: false })) Exercise: ExerciseRequest): Promise<JSON> {
    return this.ExerciseService.edit(Exercise).then((r) => {
      return this.ExerciseSerializer.serialize(r);
    }).catch(() =>
    {
      throw  new Error({
        code: '400',
        title: 'Bad Request',
        detail: 'Failed to the exercise.',
      });
    })

  }

  @Post()
  @ApiCreatedResponse({
    type: ExerciseResponse,
    isArray: false
  })
  @ApiResponse({ status: 200, description: 'The exercise has been successfully created.'})
  @ApiResponse({ status: 400, description: 'Failed to create the exercise, make sure the exercise does not exist.'})
  create(@Body(new ValidationPipe({ skipMissingProperties: true })) Exercise: ExerciseRequest): Promise<JSON> {
    return this.ExerciseService.create(Exercise).then((r) => {
      return this.ExerciseSerializer.serialize(r);
    })
  }

  @Get('')
  @ApiCreatedResponse({
    type: ExerciseResponse,
    isArray: true
  })
  @ApiResponse({ status: 200, description: 'The exercises have been successfully fetched.'})
  @ApiResponse({ status: 400, description: 'Failed to fetch the exercises, make sure the exercises exist.'})

  getAll(): Promise<JSON> {
    return this.ExerciseService.getAll().then((r) => {
      return  JSON.parse(JSON.stringify(r));

    })
  }

  @Get(':id')
  @ApiCreatedResponse({
    type: ExerciseResponse,
    isArray: false
  })
  @ApiResponse({ status: 200, description: 'The exercises has been successfully fetched.'})
  @ApiResponse({ status: 400, description: 'Failed to fetch the exercises, make sure the exercise exists.'})
  getById(@Param('id')id: number): Promise<JSON> {
    return this.ExerciseService.getById(id).then((r) => {
      return this.ExerciseSerializer.serialize(r);
    }).catch(() =>
    {
      throw  new Error({
        code: '400',
        title: 'Bad Request',
        detail: 'Failed to fetch exercise, make sure the exercise exists.',
      });
    })
  }

}
