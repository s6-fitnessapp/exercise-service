import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn
} from 'typeorm';
import {IsNumber, IsString, Length, Min, MinLength} from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";
import {ExerciseMuscleGroupEntity} from "./muscle-group.entity";

@Entity()
export class ExerciseEntity extends BaseEntity {
    @ApiProperty()
    @PrimaryGeneratedColumn()
    @IsNumber()
    @Min(1)
    id: number;
    @ApiProperty()
    @Column({unique: true})
    @Length(2, 60)
    @IsString()
    name: string;
    @ApiProperty()
    @Column('nvarchar', {default: ""})
    @Length(2, 180)
    @IsString()
    description: string;
    @ApiProperty()
    @Column('nvarchar', {default: ""})
    @Length(2, 255)
    @IsString()
    filename: string;
    @ApiProperty()
    @JoinTable()
    @OneToMany(() => ExerciseMuscleGroupEntity, (muscleGroup) => muscleGroup.exercise, {
        cascade: true,
    })
    public   muscleGroup: ExerciseMuscleGroupEntity[];
    @ApiProperty()
    @Column('nvarchar', {default: "Easy"})
    @Length(2, 60)
    @IsString()
    difficulty: string;

}