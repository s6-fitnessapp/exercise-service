import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from 'typeorm';
import {IsNumber, IsString, Length, Min, MinLength} from 'class-validator';
import {ExerciseEntity} from "./exercise.entity";


@Entity()
export class ExerciseMuscleGroupEntity extends BaseEntity {

    @PrimaryColumn()
    @IsNumber()
    @Min(1)
    id: number;

    @ManyToOne(() => ExerciseEntity, (exercise) => exercise.muscleGroup, {
        onDelete: 'CASCADE',
    })
    public exercise: ExerciseEntity;


}