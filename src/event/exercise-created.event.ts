export class ExerciseCreatedEvent {
    id: number;
    name: string;
}
