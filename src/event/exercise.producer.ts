import { Injectable } from '@nestjs/common'
import { Client, ClientKafka, Transport } from '@nestjs/microservices';

@Injectable()
export class ExerciseProducer {


  @Client({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: 'hero',
        brokers: ['kafka:9093'],
      },
      consumer: {
        groupId: 'hero-consumer'
      }
    }
  })
  kafka: ClientKafka;

  public broadcastMessage(object: any, topic: string) {
    if (process.env.NODE_ENV !== 'test') {
      return this.kafka.send(topic, { value: JSON.stringify(object) });
    }
  }
}
