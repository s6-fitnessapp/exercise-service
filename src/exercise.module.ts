import { Module } from '@nestjs/common';
import { ExerciseController } from './controller/exercise.controller';
import { ExerciseService } from './service/exercise.service';
import { ExerciseProducer } from './event/exercise.producer';

@Module({
    controllers: [ExerciseController],
    providers: [ExerciseService,ExerciseProducer]
})
export class ExerciseModule {}
