import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import 'reflect-metadata';


async function bootstrap() {
  /*  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
      transport: Transport.KAFKA,

      options: {
        client: {
          brokers: ['127.0.0.1:9093'],
        }
      }
    });*/
// Create your regular nest application.
  const app = await NestFactory.create(AppModule);

// Then combine it with your microservice
  app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        brokers: ['kafka:9093'],
      },
    },
  });
  await app.startAllMicroservicesAsync();
  await app.listen(8082);


}

bootstrap();

