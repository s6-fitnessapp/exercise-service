import {IsNumber, IsString, MinLength, Min} from 'class-validator';
import {ApiProperty} from '@nestjs/swagger';
import {ExerciseMuscleGroupEntity} from "../entity/muscle-group.entity";


export class ExerciseRequest {

    @IsNumber()
    @ApiProperty()
    @Min(1)
    id: number;

    @MinLength(2)
    @ApiProperty()
    @IsString()
    name: string;
    @MinLength(5)
    @ApiProperty()
    @IsString()
    description: string;
    @MinLength(5)
    @ApiProperty()
    @IsString()
    filename: string;
    @ApiProperty()
    muscleGroup: number[];
    @MinLength(5)
    @ApiProperty()
    @IsString()
    difficulty: string;
}