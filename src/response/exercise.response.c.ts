import { ApiProperty } from '@nestjs/swagger';


export class ExerciseResponse {
  @ApiProperty()
  id: number;
  @ApiProperty()
  name: string;

  @ApiProperty()

  description: string;

  @ApiProperty()
  filename: string;
  @ApiProperty()
  muscleGroup: string;
  @ApiProperty()
  difficulty: string;
}