import {ExerciseMuscleGroupEntity} from "../entity/muscle-group.entity";


export interface ExerciseResponse {

    description: string;
    filename: string;

    id: number;
    name: string;

    muscleGroup: ExerciseMuscleGroupEntity[];
    difficulty: string;
}