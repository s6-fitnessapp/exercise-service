import { Test, TestingModule } from '@nestjs/testing';
import { ExerciseProducer } from '../event/exercise.producer';
import { ExerciseService } from "./exercise.service";

describe('ExerciseService', () => {
  let service: ExerciseService;
  let producer: ExerciseProducer;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExerciseService, ExerciseProducer],
    }).compile();

    service = module.get<ExerciseService>(ExerciseService);
    producer = module.get<ExerciseProducer>(ExerciseProducer);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be defined', () => {
    expect(producer).toBeDefined();
  });

  it('just works ;)', () => {
    expect(producer).toBeDefined();
  });
});
