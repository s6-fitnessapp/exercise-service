import {ExerciseEntity} from "../entity/exercise.entity";
import {Injectable} from '@nestjs/common';
import {ExerciseRequest} from "../models/exercise.request";
import {DeleteResult} from 'typeorm';
import {ExerciseResponse} from '../response/exercise.response';
import {ExerciseProducer} from "../event/exercise.producer";
import {ExerciseMuscleGroupEntity} from "../entity/muscle-group.entity";

@Injectable()
export class ExerciseService {

    constructor(private readonly ExerciseProducer: ExerciseProducer) {
    }

    public async create(Exercise: ExerciseRequest): Promise<ExerciseResponse> {

        let exerciseEntity = this.createExerciseEntity(Exercise);
        this.updateMuscleGroups(Exercise, exerciseEntity);
        const exerciseResponse = await exerciseEntity.save();
        this.ExerciseProducer.broadcastMessage(exerciseResponse, 'Exercise_CREATED')
        return exerciseResponse;
    }

    public async edit(Exercise: ExerciseRequest): Promise<ExerciseResponse> {
        let exerciseEntity = this.createExerciseEntity(Exercise);
        await ExerciseMuscleGroupEntity.remove(exerciseEntity.muscleGroup)
        this.updateMuscleGroups(Exercise, exerciseEntity);
        const exerciseResponse = await exerciseEntity.save();
        this.ExerciseProducer.broadcastMessage(exerciseResponse, 'Exercise_EDITED')
        return exerciseResponse;
    }

    private updateMuscleGroups(Exercise: ExerciseRequest, exerciseEntity: ExerciseEntity) {
        for (const id of Exercise.muscleGroup) {
            const exerciseMuscleGroupEntity = new ExerciseMuscleGroupEntity();
            exerciseMuscleGroupEntity.id = id;
            exerciseEntity.muscleGroup.push(exerciseMuscleGroupEntity);
        }
    }

    private createExerciseEntity(Exercise: ExerciseRequest) {
        const exerciseEntity = new ExerciseEntity();
        exerciseEntity.name = Exercise.name;
        exerciseEntity.description = Exercise.description;
        exerciseEntity.difficulty = Exercise.difficulty;
        exerciseEntity.filename = Exercise.filename;
        exerciseEntity.muscleGroup = new Array() as Array<ExerciseMuscleGroupEntity>;
        return exerciseEntity;
    }

    public async delete(id: number): Promise<DeleteResult> {

        const exerciseResponse = await ExerciseEntity.delete(id);
        this.ExerciseProducer.broadcastMessage(exerciseResponse, 'Exercise_DELETED')
        return exerciseResponse;
    }

    public async getAll(): Promise<ExerciseResponse[]> {
        let exerciseEntities = await ExerciseEntity.find({ relations: ["muscleGroup"] })
        console.log(exerciseEntities)
        return exerciseEntities;
    }

    public async getById(id: number): Promise<ExerciseResponse> {
        return await ExerciseEntity.findOne({
            where:
                {id: id}
        });
    }

    public async getByIds(id: Array<number>): Promise<ExerciseResponse[]> {
        return await ExerciseEntity.findByIds(id, {});
    }
}